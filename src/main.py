#main.py
import quicksort

def main(argv = None):
	f = open('numbers.txt')
	input_list = f.read().rstrip('\n').split('\n')
	integer_list = list()
	for x in input_list:
		integer_list.append(int(x))
	print integer_list
	quicksort.quicksort(integer_list, 0, len(input_list) - 1)
	print integer_list

main()
