#quicksort.py

#The following is an implementation of quicksort with several different
#functions for choosing a pivot. While there are different ways of implementing
#the partitioning subroutine, this implementation will contain a specific
#version, namely where the pivot is swapped to the beginning of the array before
#the partitioning begins and finally swapped back to its rightful place after
#the partitioning has occured.

def quicksort(input_list, start, end):
	
	if end == start:
		return
	pivot = firstpivot(input_list, start)
	pivot = partition(input_list, start, end, pivot)
	if pivot != start:
		quicksort(input_list, start, pivot - 1)
	if pivot != end:
		quicksort(input_list, pivot + 1, end)

def firstpivot(input_list, start):
	return start

def partition(input_list, start, end, pivot):
	i = start + 1
	for j in range(start + 1, end + 1):
		if input_list[pivot] > input_list[j]:
			if i != j:
				swap(input_list, j, i)
			i += 1
	if i - 1 != pivot: 
		swap(input_list, i - 1 , pivot)
	return i - 1

def swap(input_list, first, second):
	temp = input_list[first]
	input_list[first] = input_list[second]
	input_list[second] = temp





